package com.gmail.jd4656.customcodes;

import com.gmail.jd4656.InventoryManager.InventoryClickHandler;
import com.gmail.jd4656.InventoryManager.InventoryManager;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_16_R3.NBTTagCompound;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.craftbukkit.v1_16_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.StringUtil;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CommandCustomVouchers implements TabExecutor {
    private CustomCodes plugin;

    CommandCustomVouchers(CustomCodes p) {
        plugin = p;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 1) {
            args = new String[]{"help"};
        }

        if (args[0].equals("help") || args[0].equals("?")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }

            Player player = (Player) sender;

            if (!sender.hasPermission("customcodes.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }

            int pageNum = 1;
            if (args.length > 1) {
                try {
                    pageNum = Integer.parseInt(args[1]);
                    if (pageNum < 1) pageNum = 1;
                } catch (NumberFormatException ignored) {}
            }

            List<TextComponent> messages = new ArrayList<>();
            messages.add(plugin.formatText("/voucher create <type> <id> - Creates a new voucher based on the item in your hand."));
            messages.add(plugin.formatText("/voucher give <voucherid> <player> <amount> <playeronly (optional)> - Gives a player a voucher. If <playeronly> is specified, voucher will only be redeemable by the player that received it."));
            messages.add(plugin.formatText("/voucher setperm <voucherid> <permission> <time (optional)> - Sets the permission a voucher will grant when redeemed."));
            messages.add(plugin.formatText("/voucher setcmd <voucherid> <eco give {user} 2000> - Sets the command that will be run when a voucher is redeemed."));
            messages.add(plugin.formatText("/voucher additem <voucherid> - Adds an item to a voucher."));
            messages.add(plugin.formatText("/voucher removeitem <voucherid> - Removes an item from a voucher."));

            List<BaseComponent> pages = plugin.pageify(messages, pageNum, "/voucher help");

            for (BaseComponent page : pages) player.spigot().sendMessage(page);

            return true;
        }

        if (args[0].equals("give")) {
            if (!sender.hasPermission("customcodes.voucher.give")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (args.length < 3) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /voucher give <voucherid> <player> <amount>");
                return true;
            }

            String voucherId = args[1];
            Player targetPlayer = Bukkit.getPlayer(args[2]);
            boolean playerOnly = (args.length > 4 && args[4].equals("true"));
            int amount = 1;

            if (args.length > 3) {
                try {
                    amount = Integer.parseInt(args[3]);
                } catch (NumberFormatException e) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid amount.");
                    return true;
                }
            }

            if (targetPlayer == null || !targetPlayer.isOnline()) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Player not found.");
                return true;
            }

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM vouchers WHERE id=?");
                pstmt.setString(1, voucherId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That voucher does not exist.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("SELECT * FROM vouchers WHERE id=?");
                pstmt.setString(1, voucherId);
                rs = pstmt.executeQuery();

                ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(rs.getString("item")));
                BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
                ItemStack item = (ItemStack) dataInput.readObject();
                dataInput.close();

                net.minecraft.server.v1_16_R3.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
                NBTTagCompound itemCompound = (nmsItem.hasTag() ? nmsItem.getTag() : new NBTTagCompound());

                if (playerOnly) itemCompound.setString("cvRedeemableBy", targetPlayer.getUniqueId().toString());

                nmsItem.setTag(itemCompound);
                ItemStack newItem = CraftItemStack.asBukkitCopy(nmsItem);
                ItemMeta meta = newItem.getItemMeta();

                String displayName = ChatColor.WHITE + meta.getDisplayName();

                meta.setDisplayName(displayName);

                List<String> lore = (meta.hasLore() ? meta.getLore() : new LinkedList<>());
                for (int i = 0; i < lore.size(); i++) {
                    lore.set(i, ChatColor.WHITE + lore.get(i));
                }

                if (!rs.getString("type").equals("reward")) {
                    lore.add(ChatColor.RED + "Right click to redeem!");
                }
                if (playerOnly) {
                    lore.add(ChatColor.RED + "Only redeemable by " + ChatColor.GOLD + targetPlayer.getName());
                }

                meta.setLore(lore);
                newItem.setItemMeta(meta);

                newItem.setAmount(amount);


                String voucherName = null;
                if (newItem.hasItemMeta()) {
                    if (meta.hasDisplayName()) voucherName = meta.getDisplayName().trim();
                }

                pstmt.close();

                sender.sendMessage(ChatColor.GOLD + "You've given " + targetPlayer.getName() + " " + amount + (amount == 1 ? " voucher." : " vouchers."));
                if (plugin.freeSpace(targetPlayer.getInventory(), newItem) < amount) {
                    targetPlayer.getWorld().dropItem(targetPlayer.getLocation().clone().add(0, 1, 0), newItem);
                } else {
                    targetPlayer.getInventory().addItem(newItem);
                }

                targetPlayer.sendMessage(ChatColor.GOLD + "You've received " + ChatColor.RED + amount + ChatColor.GOLD + " " + (voucherName == null ? "" : voucherName) + ChatColor.GOLD + (amount == 1 ? " voucher." : " vouchers."));

                return true;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("setitem")) {
            if (!sender.hasPermission("customcodes.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }
            if (args.length < 2) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /voucher setitem <voucherid>");
                return true;
            }

            Player player = (Player) sender;
            String voucherId = args[1];

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM vouchers WHERE id=?");
                pstmt.setString(1, voucherId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A voucher with that id does not exist.");
                    pstmt.close();
                    return true;
                }

                ItemStack item = player.getInventory().getItemInMainHand();
                if (item == null || item.getType().equals(Material.AIR)) {
                    player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You're not holding an item.");
                    return true;
                }

                net.minecraft.server.v1_16_R3.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
                NBTTagCompound itemCompound = (nmsItem.hasTag() ? nmsItem.getTag() : new NBTTagCompound());

                itemCompound.setString("cvId", voucherId);

                nmsItem.setTag(itemCompound);
                ItemStack newItem = CraftItemStack.asBukkitCopy(nmsItem);

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

                dataOutput.writeObject(newItem);
                dataOutput.close();

                pstmt = c.prepareStatement("UPDATE vouchers SET item=? WHERE id=?");
                pstmt.setString(1, Base64Coder.encodeLines(outputStream.toByteArray()));
                pstmt.setString(2, voucherId);
                pstmt.executeUpdate();
                pstmt.close();

                sender.sendMessage(ChatColor.GOLD + "You've updated the item for that voucher.");
                return true;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("create")) {
            if (!sender.hasPermission("customcodes.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }
            if (args.length < 3) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /vouchers create <type> <voucherid>");
                return true;
            }

            Player player = (Player) sender;
            String type = args[1];
            String voucherId = args[2];

            if (!type.equals("item") && !type.equals("permission") && !type.equals("temppermission") && !type.equals("command") && !type.equals("reward")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid type.");
                return true;
            }

            try {
               Connection c = plugin.getConnection();
               PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM vouchers WHERE id=?");
               pstmt.setString(1, voucherId);
               ResultSet rs = pstmt.executeQuery();

               if (rs.getInt("count(*)") > 0) {
                   sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A voucher with that id already exists.");
                   pstmt.close();
                   return true;
               }

                ItemStack item = player.getInventory().getItemInMainHand();
                if (item == null || item.getType().equals(Material.AIR)) {
                    player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You're not holding an item.");
                    return true;
                }

                net.minecraft.server.v1_16_R3.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
                NBTTagCompound itemCompound = (nmsItem.hasTag() ? nmsItem.getTag() : new NBTTagCompound());

                itemCompound.setString("cvId", voucherId);

                nmsItem.setTag(itemCompound);
                ItemStack newItem = CraftItemStack.asBukkitCopy(nmsItem);

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

                dataOutput.writeObject(newItem);
                dataOutput.close();

                pstmt = c.prepareStatement("INSERT INTO vouchers (id, item, type) VALUES (?, ?, ?)");
                pstmt.setString(1, voucherId);
                pstmt.setString(2, Base64Coder.encodeLines(outputStream.toByteArray()));
                pstmt.setString(3, type);
                pstmt.executeUpdate();
                pstmt.close();

                plugin.cachedVouchers.add(voucherId);

                sender.sendMessage(ChatColor.GOLD + "You've created a voucher with the id " + ChatColor.RED + voucherId);
                return true;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("remove")) {
            if (!sender.hasPermission("customcodes.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (args.length < 2) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /vouchers remove <voucherid>");
                return true;
            }

            String voucherId = args[1];

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM vouchers WHERE id=?");
                pstmt.setString(1, voucherId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That voucher does not exist.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("DELETE FROM vouchers WHERE id=?");
                pstmt.setString(1, voucherId);
                pstmt.executeUpdate();

                pstmt = c.prepareStatement("DELETE FROM voucherItems WHERE voucher=?");
                pstmt.setString(1, voucherId);
                pstmt.executeUpdate();

                plugin.cachedVouchers.remove(voucherId);
                pstmt.close();
                sender.sendMessage(ChatColor.GOLD + "That voucher has been deleted.");
                return true;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("removeitem")) {
            if (!sender.hasPermission("customcodes.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }
            if (args.length < 2) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /voucher removeitem <voucherid>");
                return true;
            }

            Player player = (Player) sender;
            String voucherId = args[1];

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM vouchers WHERE id=?");
                pstmt.setString(1, voucherId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That voucher does not exist.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("SELECT count(*) FROM voucherItems WHERE voucher=?");
                pstmt.setString(1, voucherId);
                rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That voucher has no items added.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("SELECT * FROM voucherItems WHERE voucher=?");
                pstmt.setString(1, voucherId);
                rs = pstmt.executeQuery();

                InventoryManager manager = new InventoryManager(ChatColor.RED + ChatColor.BOLD.toString() + "Click an item to remove it", 54, plugin);

                int count = 0;
                while (rs.next()) {
                    ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(rs.getString("item")));
                    BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);

                    ItemStack curItem = (ItemStack) dataInput.readObject();
                    dataInput.close();

                    net.minecraft.server.v1_16_R3.ItemStack nmsItem = CraftItemStack.asNMSCopy(curItem);
                    NBTTagCompound itemCompound = (nmsItem.hasTag() ? nmsItem.getTag() : new NBTTagCompound());

                    itemCompound.setString("cvItemId", "id");

                    nmsItem.setTag(itemCompound);
                    ItemStack newItem = CraftItemStack.asBukkitCopy(nmsItem);

                    manager.withItem(count, newItem);
                    count++;
                }

                manager.withEventHandler(new InventoryClickHandler() {
                    @Override
                    public void handle(InventoryClickEvent event) {
                        Player player = (Player) event.getWhoClicked();
                        ItemStack clicked = event.getCurrentItem();
                        if (clicked.getType().equals(Material.AIR)) {
                            event.setCancelled(true);
                            return;
                        }

                        InventoryManager confirm = new InventoryManager(ChatColor.GOLD + "Confirm deletion", 54, plugin);

                        ItemStack yes = new ItemStack(Material.LIME_CONCRETE, 1);
                        ItemMeta yesMeta = yes.getItemMeta();
                        yesMeta.setDisplayName("Confirm");
                        yes.setItemMeta(yesMeta);

                        ItemStack no = new ItemStack(Material.RED_CONCRETE, 1);
                        ItemMeta noMeta = yes.getItemMeta();
                        noMeta.setDisplayName("Cancel");
                        no.setItemMeta(noMeta);

                        confirm.withItem(0, clicked);
                        confirm.withItem(1, yes);
                        confirm.withItem(2, no);

                        confirm.withEventHandler(new InventoryClickHandler() {
                            @Override
                            public void handle(InventoryClickEvent event1) {
                                Player player2 = (Player) event1.getWhoClicked();
                                ItemStack clicked1 = event1.getCurrentItem();
                                if (!clicked1.getType().equals(Material.LIME_CONCRETE) && !clicked1.getType().equals(Material.RED_CONCRETE)) {
                                    event1.setCancelled(true);
                                    return;
                                }

                                if (clicked1.getType().equals(Material.LIME_CONCRETE)) {

                                    net.minecraft.server.v1_16_R3.ItemStack nmsItem = CraftItemStack.asNMSCopy(clicked);
                                    NBTTagCompound itemCompound = (nmsItem.hasTag() ? nmsItem.getTag() : new NBTTagCompound());
                                    String itemId = itemCompound.getString("cvItemId");

                                    try {
                                        Connection c = plugin.getConnection();
                                        PreparedStatement pstmt = c.prepareStatement("DELETE FROM voucherItems WHERE id=?");
                                        pstmt.setString(1, itemId);
                                        pstmt.executeUpdate();
                                        pstmt.close();

                                        player.closeInventory();
                                        player.sendMessage(ChatColor.GOLD + "You've removed that item.");
                                    } catch (Exception e) {
                                        sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                                        plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                                        e.printStackTrace();
                                    }
                                }

                                if (clicked1.getType().equals(Material.RED_CONCRETE)) player.closeInventory();
                            }
                        });

                        confirm.show(player);
                    }
                });

                manager.show(player);
                
                pstmt.close();
                return true;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("additem")) {
            if (!sender.hasPermission("customcodes.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }
            if (args.length < 2) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /voucher additem <voucherid>");
                return true;
            }

            Player player = (Player) sender;
            String voucherId = args[1];

            ItemStack item = player.getInventory().getItemInMainHand();
            if (item == null || item.getType().equals(Material.AIR)) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You're not holding an item.");
                return true;
            }

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM vouchers WHERE id=?");
                pstmt.setString(1, voucherId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That voucher does not exist.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("SELECT * FROM vouchers WHERE id=?");
                pstmt.setString(1, voucherId);
                rs = pstmt.executeQuery();

                if (!rs.getString("type").equals("item")) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This voucher is not an item voucher.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("SELECT count(*) FROM voucherItems WHERE voucher=?");
                pstmt.setString(1, voucherId);
                rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") >= 54) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This voucher can not have more than 54 items added.");
                    pstmt.close();
                    return true;
                }

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

                dataOutput.writeObject(item);
                dataOutput.close();

                String randomId = plugin.randomId();

                pstmt = c.prepareStatement("INSERT INTO voucherItems (id, voucher, item) VALUES (?, ?, ?)");
                pstmt.setString(1, randomId);
                pstmt.setString(2, voucherId);
                pstmt.setString(3, Base64Coder.encodeLines(outputStream.toByteArray()));
                pstmt.executeUpdate();
                pstmt.close();

                sender.sendMessage(ChatColor.GOLD + "You've added this item to the voucher.");
                return true;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("setcmd")) {
            if (!sender.hasPermission("customcodes.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (args.length < 3) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /voucher setcmd <voucherid> <eco give {user} 2000>");
                return true;
            }

            String voucherId = args[1];
            String cmd = "";
            for (int i=2; i < args.length; i++) cmd += args[i] + " ";
            cmd = cmd.trim();

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM vouchers WHERE id=?");
                pstmt.setString(1, voucherId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That voucher does not exist.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("SELECT * FROM vouchers WHERE id=?");
                pstmt.setString(1, voucherId);
                rs = pstmt.executeQuery();

                if (!rs.getString("type").equals("command")) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This voucher is not a command voucher.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("UPDATE vouchers SET command=? WHERE id=?");
                pstmt.setString(1, cmd);
                pstmt.setString(2, voucherId);
                pstmt.executeUpdate();
                pstmt.close();

                sender.sendMessage(ChatColor.GOLD + "You've set the command for that voucher.");
                return true;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("setperm")) {
            if (!sender.hasPermission("customcodes.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (args.length < 3) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /voucher setperm <voucherid> <permission> <time (optional)>");
                return true;
            }

            String voucherId = args[1];
            String permission = args[2];
            String time = null;
            if (args.length > 3) time = args[3];
            if (time != null && plugin.stringToTime(time) == 0) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid time format.");
                return true;
            }

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM vouchers WHERE id=?");
                pstmt.setString(1, voucherId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That voucher does not exist.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("SELECT * FROM vouchers WHERE id=?");
                pstmt.setString(1, voucherId);
                rs = pstmt.executeQuery();

                if (!rs.getString("type").equals("permission")) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This voucher is not a permission voucher.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("UPDATE vouchers SET permission=?, permTime=? WHERE id=?");
                pstmt.setString(1, permission);
                pstmt.setString(2, time);
                pstmt.setString(3, voucherId);

                pstmt.executeUpdate();
                pstmt.close();

                sender.sendMessage(ChatColor.GOLD + "The permission has been set for that voucher.");
                return true;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        return true;
    }

    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        List<String> tabComplete = new ArrayList<>();

        if (sender.hasPermission("customcodes.admin")) {
            if (args.length == 1) {
                tabComplete.add("create");
                tabComplete.add("remove");
                tabComplete.add("setitem");
                tabComplete.add("give");
                tabComplete.add("setperm");
                tabComplete.add("setcmd");
                tabComplete.add("additem");
                tabComplete.add("removeitem");
            }
            if (args.length == 2) {
                if (args[0].equals("setitem") || args[0].equals("give") || args[0].equals("setperm") || args[0].equals("setcmd") || args[0].equals("additem") || args[0].equals("removeitem")) tabComplete.addAll(plugin.cachedVouchers);
                if (args[0].equals("create")) {
                    tabComplete.add("permission");
                    tabComplete.add("command");
                    tabComplete.add("item");
                    tabComplete.add("reward");
                }
                if (args[0].equals("remove")) tabComplete.addAll(plugin.cachedVouchers);
            }
            if (args.length == 3) {
                if (args[0].equals("setperm")) tabComplete.add("<permission>");
                if (args[0].equals("setcmd")) tabComplete.add("<command>");
                if (args[0].equals("create")) tabComplete.add("<voucherid>");
                if (args[0].equals("give")) return null;
            }
            if (args.length == 4) {
                if (args[0].equals("setperm")) tabComplete.add("<time>");
                if (args[0].equals("give")) tabComplete.add("<amount>");
            }
            if (args.length == 5) {
                if (args[0].equals("give")) tabComplete.add("true");
            }
        }

        return (args.length > 0) ? StringUtil.copyPartialMatches(args[args.length - 1], tabComplete, new ArrayList<>()) : null;
    }
}
