package com.gmail.jd4656.customcodes;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.util.StringUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CommandGrant implements TabExecutor {
    private CustomCodes plugin;

    CommandGrant(CustomCodes p) {
        plugin = p;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("customcodes.admin")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }
        if (args.length < 2) {
            sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /grant <username> <code> - Gives a user the permission assigned to a code.");
            return true;
        }

        String code = args[1];

        try {
            Connection c = plugin.getConnection();
            PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM codes WHERE code=?");
            pstmt.setString(1, code);
            ResultSet rs = pstmt.executeQuery();

            if (rs.getInt("count(*)") < 1) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That code does not exist.");
                pstmt.close();
                return true;
            }

            pstmt = c.prepareStatement("SELECT * FROM codes WHERE code=?");
            pstmt.setString(1, code);
            rs = pstmt.executeQuery();

            String permission = rs.getString("permission");

            if (permission == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That code does not have a permission set.");
                pstmt.close();
                return true;
            }

            Bukkit.dispatchCommand(sender, "lp user " + args[0] + " permission settemp customcodes.code." + permission + " true 60d world=worldtowny");
            Bukkit.dispatchCommand(sender, "mail send " + args[0] + " You've been given access to the code " + code + ". Do /codes redeem " + code);
            pstmt.close();
            return true;
        } catch (Exception e) {
            sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
            plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
            return true;
        }
    }
    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        List<String> tabComplete = new ArrayList<>();
        if (args.length == 1) return null;
        if (args.length == 2) tabComplete.addAll(plugin.cachedCodes);
        return (args.length > 0) ? StringUtil.copyPartialMatches(args[args.length - 1], tabComplete, new ArrayList<>()) : null;
    }
}
