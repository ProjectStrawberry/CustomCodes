package com.gmail.jd4656.customcodes;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.Date;

public class CustomCodes extends JavaPlugin {
    private CustomCodes plugin;
    private File dbFile;
    private Connection conn = null;
    private Random rng = new Random();
    private BufferedWriter bufferedWriter;

    List<String> cachedCodes = new ArrayList<>();
    List<String> cachedActions = new ArrayList<>();
    List<String> cachedVouchers = new ArrayList<>();

    @Override
    public void onEnable() {
        plugin = this;
        plugin.getDataFolder().mkdirs();

        dbFile = new File(plugin.getDataFolder(), "CustomCodes.db");
        if (!dbFile.exists()) {
            try {
                dbFile.createNewFile();
            } catch (IOException e) {
                plugin.getLogger().severe("File write error: CustomCodes.db");
            }
        }

        try {
            File logFile = new File(plugin.getDataFolder(), "vouchers.log");
            if (!logFile.exists()) {
                logFile.getParentFile().mkdirs();
                logFile.createNewFile();
            }
            bufferedWriter = new BufferedWriter(new FileWriter(logFile, true));
        } catch (Exception e) {
            plugin.getLogger().severe("File write error: vouchers.log");
            return;
        }

        try {
            Statement stmt;
            Connection c = plugin.getConnection();

            stmt = c.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS codes (code TEXT, expiresAt INTEGER, uses INTEGER, playerCd INTEGER, globalCd INTEGER, lastUsed INTEGER, permission TEXT)");

            stmt = c.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS actions (id TEXT, code TEXT, type TEXT, action TEXT)");

            stmt = c.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS players (uuid TEXT, code TEXT, lastUsed INTEGER)");

            stmt = c.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS vouchers (id TEXT, item TEXT, type TEXT, permission TEXT, permTime TEXT, command TEXT)");

            stmt = c.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS voucherItems (id TEXT, voucher TEXT, item TEXT)");

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT count(*) FROM codes");

            if (rs.getInt("count(*)") > 0) {
                stmt = c.createStatement();
                rs = stmt.executeQuery("SELECT * FROM codes");
                while (rs.next()) {
                    cachedCodes.add(rs.getString("code"));
                }
            }

            stmt = c.createStatement();
            rs = stmt.executeQuery("SELECT count(*) FROM actions");

            if (rs.getInt("count(*)") > 0) {
                stmt = c.createStatement();
                rs = stmt.executeQuery("SELECT * FROM actions");
                while (rs.next()) {
                    cachedActions.add(rs.getString("id"));
                }
            }

            stmt = c.createStatement();
            rs = stmt.executeQuery("SELECT count(*) FROM vouchers");

            if (rs.getInt("count(*)") > 0) {
                stmt = c.createStatement();
                rs = stmt.executeQuery("SELECT * FROM vouchers");
                while (rs.next()) cachedVouchers.add(rs.getString("id"));
            }

            stmt.close();
        } catch (Exception e) {
            getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
            getLogger().warning("CustomCodes failed to load.");
            return;
        }

        getServer().getPluginManager().registerEvents(new EventListeners(this), this);

        this.getCommand("codes").setExecutor(new CommandCustomCodes(this));
        this.getCommand("grant").setExecutor(new CommandGrant(this));
        this.getCommand("vouchers").setExecutor(new CommandCustomVouchers(this));

        getLogger().info("CustomCodes loaded.");
    }

    @Override
    public void onDisable() {
        try {
            plugin.getConnection().close();
        } catch (Exception e) {
            getLogger().warning("Error when disabling CustomCodes: " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
        }
    }

    Connection getConnection() throws Exception {
        if (conn == null ) {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + plugin.dbFile);
        }
        return conn;
    }

    boolean sameItem(ItemStack item1, ItemStack item2) {
        ItemStack clonedItem1 = item1.clone();
        ItemStack clonedItem2 = item2.clone();
        clonedItem1.setAmount(1);
        clonedItem2.setAmount(1);

        if (clonedItem1.hasItemMeta() && clonedItem2.hasItemMeta()) {
            ItemMeta meta1 = clonedItem1.getItemMeta();
            ItemMeta meta2 = clonedItem2.getItemMeta();

            if ((meta1.hasItemFlag(ItemFlag.HIDE_POTION_EFFECTS) && meta1.hasDisplayName()) && (meta2.hasItemFlag(ItemFlag.HIDE_POTION_EFFECTS) && meta2.hasDisplayName())) {
                if (meta1.getDisplayName().equals("Indefinable Distillate") && meta2.getDisplayName().equals("Indefinable Distillate")) return true;
                if (meta1.getDisplayName().equals("Indefinable Brew") && meta2.getDisplayName().equals("Indefinable Brew")) return true;
            }
        }

        return (clonedItem1.hashCode() == clonedItem2.hashCode());
    }

    int freeSpace(Inventory inv, ItemStack item) {
        int free = 0;

        for (ItemStack curItem : inv.getStorageContents()) {
            if (curItem == null) {
                free += item.getMaxStackSize();
                continue;
            }
            if (!plugin.sameItem(curItem, item)) continue;
            free += (curItem.getMaxStackSize() - curItem.getAmount());
        }

        return free;
    }

    public List<ItemStack> removeItems(Inventory inv, ItemStack item, int amountToRemove) {
        ItemStack[] contents = inv.getContents();
        List<ItemStack> removedItems = new ArrayList<>();
        int curAmount = 0;
        int slot = 0;

        for (ItemStack curItem : contents) {
            slot++;
            if (curItem == null || !sameItem(item, curItem)) continue;
            if ((curItem.getAmount() + curAmount) <= amountToRemove) {
                curAmount += curItem.getAmount();
                if (inv instanceof PlayerInventory && slot > 36) {
                    inv.setItem((slot - 1), null);
                } else {
                    inv.removeItem(curItem);
                }
                removedItems.add(curItem);
            } else {
                int removeAmount = (amountToRemove - curAmount);
                int newSize = curItem.getAmount() - removeAmount;
                curItem.setAmount(newSize);
                ItemStack clonedItem = curItem.clone();
                clonedItem.setAmount(removeAmount);
                if (inv instanceof PlayerInventory && slot > 36) {
                    inv.setItem((slot - 1), clonedItem);
                }
                removedItems.add(clonedItem);
                curAmount += removeAmount;
            }

            if (curAmount == amountToRemove) break;
        }

        return removedItems;
    }

    void log(String message) {
        Date date = Calendar.getInstance().getTime();
        Timestamp time = new Timestamp(date.getTime());
        try {
            bufferedWriter.write("[" + time.toString() + "] " + message);
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (Exception e) {
            plugin.getLogger().warning("Error writing log file.");
            e.printStackTrace();
        }
    }

    long stringToTime(String time) {
        String validModifiers = "smhd";
        char timeModifier = time.charAt(time.length() - 1);
        int timeNumber;
        long timeDuration = 0;
        if (validModifiers.indexOf(timeModifier) < 0) {
            return 0;
        }

        try {
            timeNumber = Integer.parseInt(time.substring(0, time.length() - 1));
        } catch (NumberFormatException e) {
            return 0;
        }


        if (timeModifier == 's') {
            timeDuration = timeNumber * 1000L;
        }
        if (timeModifier == 'm') {
            timeDuration = timeNumber * 60 * 1000L;
        }
        if (timeModifier == 'h') {
            timeDuration = timeNumber * 60 * 60 * 1000L;
        }
        if (timeModifier == 'd') {
            timeDuration = timeNumber * 24 * 60 * 60 * 1000L;
        }

        return timeDuration;
    }

    String convertTime(long ms) {
        long seconds = (ms / 1000) % 60L;
        long minutes = (ms / (1000 * 60)) % 60L;
        long hours = (ms / (1000 * 60 * 60)) % 24L;
        long days = (ms / (1000 * 60 * 60 * 24L));

        String reply = "";
        if (days > 0) {
            reply += days + (days == 1 ? " day " : " days ");
        }
        if (hours > 0) {
            reply += hours + (hours == 1 ? " hour " : " hours ");
        }
        if (minutes > 0) {
            reply += minutes + (minutes == 1 ? " minute " : " minutes ");
        }
        if (seconds > 0) {
            reply += seconds + (seconds == 1 ? " second " : " seconds ");
        }

        return reply.trim();
    }

    List<BaseComponent> pageify(List messages, int pageNumber, String pageCmd) {
        int pageSize = 8;
        List<BaseComponent> page = new ArrayList<>();

        int totalPages = messages.size() / pageSize + ((messages.size() % pageSize == 0) ? 0 : 1);
        if (totalPages < pageNumber) pageNumber = totalPages;

        int count = 0;
        int curPageNum;

        page.add(new TextComponent(ChatColor.DARK_GREEN + "Page " + ChatColor.GOLD + pageNumber + ChatColor.DARK_GREEN + " of " + ChatColor.GOLD + totalPages));

        for (Object curPage : messages) {
            count++;
            curPageNum = count / pageSize + ((count % pageSize == 0) ? 0 : 1);
            if (curPageNum != pageNumber) continue;
            if (!(curPage instanceof BaseComponent)) curPage = new TextComponent((String) curPage);
            page.add((BaseComponent) curPage);
        }

        TextComponent footer = new TextComponent();
        TextComponent backButton = new TextComponent(ChatColor.DARK_GREEN + "[Last Page]");
        if (pageNumber > 1) backButton.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, pageCmd + " " + (pageNumber - 1)));
        TextComponent nextButton = new TextComponent(ChatColor.DARK_GREEN + "[Next Page]");
        if (pageNumber < totalPages) nextButton.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, pageCmd + " " + (pageNumber + 1)));

        footer.addExtra(backButton);
        footer.addExtra(" | ");
        footer.addExtra(nextButton);

        page.add(footer);

        return page;
    }

    TextComponent formatText(String message) {
        TextComponent component = new TextComponent(message);
        component.setColor(ChatColor.GOLD.asBungee());
        return component;
    }

    String randomId() {
        StringBuilder str = new StringBuilder();
        String characters = "abcdefghijklmnopqrstuvWxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        for (int i = 0; i < 6; i++) str.append(characters.charAt(rng.nextInt(characters.length())));
        return str.toString();
    }
}
