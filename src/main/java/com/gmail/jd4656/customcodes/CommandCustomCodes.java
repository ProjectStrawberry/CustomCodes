package com.gmail.jd4656.customcodes;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.command.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.StringUtil;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class CommandCustomCodes implements TabExecutor {
    private CustomCodes plugin;

    CommandCustomCodes(CustomCodes p) {
        plugin = p;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 1) {
            args = new String[]{"help"};
        }

        if (args[0].equals("help") || args[0].equals("?")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }

            Player player = (Player) sender;

            int pageNum = 1;
            if (args.length > 1) {
                try {
                    pageNum = Integer.parseInt(args[1]);
                    if (pageNum < 1) pageNum = 1;
                } catch (NumberFormatException ignored) {}
            }

            List<TextComponent> messages = new ArrayList<>();
            messages.add(plugin.formatText("/code redeem <code> - Redeems a code."));
            if (sender.hasPermission("customcodes.admin")) {
                messages.add(plugin.formatText("/code create <code> - Creates a new code."));
                messages.add(plugin.formatText("/code list codes - Lists all codes."));
                messages.add(plugin.formatText("/code list actions <code> - Lists all actions for a code."));
                messages.add(plugin.formatText("/code remove code <code> - Removes a code."));
                messages.add(plugin.formatText("/code remove action <actionid> - Removes an action from a code."));
                messages.add(plugin.formatText("/code set uses <code> <amount> - Sets the number of times this code can be redeemed."));
                messages.add(plugin.formatText("/code set playercd <code> <cooldown (1s/m/h/d)> - Sets the player cooldown for this code."));
                messages.add(plugin.formatText("/code set globalcd <code> <cooldown (1s/m/h/d)> - Sets the global cooldown for this code."));
                messages.add(plugin.formatText("/code set expires <code> <time (1s/m/h/d)> - Sets when this code expires."));
                messages.add(plugin.formatText("/code set permission <code> <permission> - Sets the permission required to redeem this code."));
                messages.add(plugin.formatText("/code set action cmd <code> <give {player} dirt 64> - Adds a command that will be run when this code is redeemed. {player} will be substituted with the name of the redeeming player."));
                messages.add(plugin.formatText("/code set action item <code> - Adds the item in your hand to this codde."));
                messages.add(plugin.formatText("/code set action msg <code> <message> - Adds a message that will be sent to the player when this code is redeemed."));
            }

            List<BaseComponent> pages = plugin.pageify(messages, pageNum, "/code help");

            for (BaseComponent page : pages) player.spigot().sendMessage(page);

            return true;
        }
        
        if (args[0].equals("redeem")) {
            if (!sender.hasPermission("customcodes.use")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }
            if (args.length < 2) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /code redeem <code>");
                return true;
            }

            Player player = (Player) sender;
            String code = args[1];

            try {
                Connection c = plugin.getConnection();
                ResultSet rs;
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM codes WHERE code=?");
                pstmt.setString(1, code);
                rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That code does not exist.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("SELECT * FROM codes WHERE code=?");
                pstmt.setString(1, code);

                rs = pstmt.executeQuery();

                long expiresAt = rs.getLong("expiresAt");
                int uses = rs.getInt("uses");
                long playerCd = rs.getLong("playerCd");
                long globalCd = rs.getLong("globalCd");
                long lastUsed = rs.getLong("lastUsed");
                String permission = rs.getString("permission");

                if (permission != null && !player.hasPermission("customcodes.code." + permission)) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to redeem this code.");
                    pstmt.close();
                    return true;
                }

                if (uses < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That code has no more uses left.");
                    pstmt.close();
                    return true;
                }

                if (expiresAt > 0 && expiresAt < System.currentTimeMillis()) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That code has expired.");
                    pstmt.close();
                    return true;
                }

                if (globalCd > 0 && (System.currentTimeMillis() - lastUsed) < globalCd) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That code has been used recently, it may not be used for another " + plugin.convertTime(globalCd - (System.currentTimeMillis() - lastUsed)));
                    pstmt.close();
                    return true;
                }

                if (playerCd > 0) {
                    boolean exists = false;
                    long lastUsedPlayer = 0;
                    pstmt = c.prepareStatement("SELECT count(*) FROM players WHERE uuid=? AND code=?");
                    pstmt.setString(1, player.getUniqueId().toString());
                    pstmt.setString(2, code);

                    rs = pstmt.executeQuery();

                    if (rs.getInt("count(*)") > 0) {
                        pstmt = c.prepareStatement("SELECT * FROM players WHERE uuid=? AND code=?");
                        pstmt.setString(1, player.getUniqueId().toString());
                        pstmt.setString(2, code);

                        rs = pstmt.executeQuery();

                        lastUsedPlayer = rs.getLong("lastUsed");
                        exists = true;
                    }

                    if (lastUsedPlayer > 0 && (System.currentTimeMillis() - lastUsedPlayer) < playerCd) {
                        sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You've used that code. You may not use it for another " + plugin.convertTime(playerCd - (System.currentTimeMillis() - lastUsedPlayer)));
                        pstmt.close();
                        return true;
                    }

                    if (!exists) {
                        pstmt = c.prepareStatement("INSERT INTO players (uuid, code, lastUsed) VALUES (?, ?, ?)");
                        pstmt.setString(1, player.getUniqueId().toString());
                        pstmt.setString(2, code);
                        pstmt.setLong(3, System.currentTimeMillis());
                        pstmt.executeUpdate();
                    } else {
                        pstmt = c.prepareStatement("UPDATE players SET lastUsed=? WHERE uuid=? AND code=?");
                        pstmt.setLong(1, System.currentTimeMillis());
                        pstmt.setString(2, player.getUniqueId().toString());
                        pstmt.setString(3, code);
                        pstmt.executeUpdate();
                    }
                }

                pstmt = c.prepareStatement("UPDATE codes SET uses = uses - 1, lastUsed=? WHERE code=?");
                pstmt.setLong(1, System.currentTimeMillis());
                pstmt.setString(2, code);
                pstmt.executeUpdate();

                pstmt = c.prepareStatement("SELECT * FROM actions WHERE code=?");
                pstmt.setString(1, code);
                rs = pstmt.executeQuery();

                boolean droppedItems = false;

                while (rs.next()) {
                    String actionType = rs.getString("type");
                    if (actionType.equals("cmd")) {
                        ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();

                        String str = rs.getString("action");
                        str = str.replaceAll("\\{user}", player.getName());
                        str = str.replaceAll("\\{player}", player.getName());
                        str = str.replaceAll("\\{name}", player.getName());

                        Bukkit.dispatchCommand(console, str);
                    }
                    if (actionType.equals("msg")) {
                        player.sendMessage(ChatColor.DARK_RED + "[CustomCodes] " + ChatColor.GOLD + rs.getString("action"));
                    }
                    if (actionType.equals("item")) {
                        ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(rs.getString("action")));
                        BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);


                        ItemStack curItem = (ItemStack) dataInput.readObject();
                        dataInput.close();

                        HashMap excess = player.getInventory().addItem(curItem);
                        if (!excess.isEmpty()) {
                            for (Object entry : excess.values()) {
                                player.getWorld().dropItem(player.getLocation().clone().add(0, 1, 0), (ItemStack) entry);
                                droppedItems = true;
                            }
                        }
                    }
                }

                player.sendMessage(ChatColor.DARK_RED + "[CustomCodes] " + ChatColor.GOLD + "You've redeemed the code " + ChatColor.RED + code);

                if (droppedItems) {
                    player.sendMessage(ChatColor.GOLD + "Your inventory did not have space for all of the items. Some items have been dropped at your feet.");
                }

                pstmt.close();
                return true;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("list")) {
            if (!sender.hasPermission("customcodes.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }

            if (args.length < 2) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /code list <codes | actions> - Lists all codes or actions.");
                return true;
            }

            if (args[1].equals("codes")) {
                try {
                    Connection c = plugin.getConnection();
                    ResultSet rs;
                    Statement stmt = c.createStatement();

                    rs = stmt.executeQuery("SELECT count(*) FROM codes");

                    if (rs.getInt("count(*)") < 1) {
                        sender.sendMessage(ChatColor.GOLD + "There are no codes added.");
                        stmt.close();
                        return true;
                    }

                    stmt = c.createStatement();
                    rs = stmt.executeQuery("SELECT * FROM codes");

                    sender.sendMessage(ChatColor.DARK_GREEN + "Custom Codes:");

                    while (rs.next()) {
                        long expiresAt = rs.getLong("expiresAt");
                        String expiresAtStr;
                        if (expiresAt == 0) {
                            expiresAtStr = ChatColor.GOLD + "Never";
                        } else {
                            expiresAtStr = (rs.getLong("expiresAt") < System.currentTimeMillis() ? ChatColor.DARK_RED + "EXPIRED" : ChatColor.GOLD + plugin.convertTime(rs.getLong("expiresAt") - System.currentTimeMillis()));
                        }
                        sender.sendMessage(ChatColor.DARK_GREEN + "Code: " + ChatColor.GOLD + rs.getString("code") + ChatColor.DARK_GREEN +
                                " Expires: " + expiresAtStr + ChatColor.DARK_GREEN + " Remaining uses: " + ChatColor.GOLD + rs.getInt("uses") +
                                (rs.getString("permission") != null ? ChatColor.DARK_GREEN + " Permission: " + rs.getString("permission") : ""));
                    }
                    stmt.close();
                    return true;
                } catch (Exception e) {
                    sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                    plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                    return true;
                }
            }

            if (args[1].equals("actions")) {
                if (args.length < 3) {
                    sender.sendMessage(ChatColor.GOLD + "Usage: /code list actions <code>");
                    return true;
                }

                String code = args[2];

                try {
                    Connection c = plugin.getConnection();
                    ResultSet rs;
                    PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM codes WHERE code=?");
                    pstmt.setString(1, code);

                    rs = pstmt.executeQuery();

                    if (rs.getInt("count(*)") < 1) {
                        sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That code does not exist.");
                        pstmt.close();
                        return true;
                    }

                    pstmt = c.prepareStatement("SELECT count(*) FROM actions WHERE code=?");
                    pstmt.setString(1, code);

                    rs = pstmt.executeQuery();

                    if (rs.getInt("count(*)") < 1) {
                        sender.sendMessage(ChatColor.GOLD + "There are no actions added for this code.");
                        pstmt.close();
                        return true;
                    }

                    pstmt = c.prepareStatement("SELECT * FROM actions WHERE code=?");
                    pstmt.setString(1, code);

                    rs = pstmt.executeQuery();

                    sender.sendMessage(ChatColor.DARK_GREEN + "Actions for code " + ChatColor.GOLD + code + ":");
                    while (rs.next()) {
                        String action;
                        if (rs.getString("type").equals("item")) {
                            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(rs.getString("action")));
                            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);


                            ItemStack curItem = (ItemStack) dataInput.readObject();
                            dataInput.close();
                            action = curItem.getType().toString();
                        } else {
                            action = rs.getString("action");
                        }
                        sender.sendMessage(ChatColor.DARK_GREEN + "Action ID: " + ChatColor.GOLD + rs.getString("id") + ChatColor.DARK_GREEN + " Type: " +
                                ChatColor.GOLD + rs.getString("type") + ChatColor.DARK_GREEN + " Action: " + ChatColor.GOLD + action);
                    }
                    pstmt.close();
                    return true;
                } catch (Exception e) {
                    sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                    plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                    return true;
                }
            }
        }

        if (args[0].equals("remove")) {
            if (!sender.hasPermission("customcodes.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "CustomCodes remove commands:");
                removeHelp(sender);
                return true;
            }

            if (args[1].equals("code")) {
                if (args.length < 3) {
                    sender.sendMessage(ChatColor.GOLD + "Usage: /code remove code <code>");
                    return true;
                }

                String code = args[2];

                try {
                    Connection c = plugin.getConnection();
                    ResultSet rs;
                    PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM codes WHERE code=?");
                    pstmt.setString(1, code);

                    rs = pstmt.executeQuery();

                    if (rs.getInt("count(*)") < 1) {
                        sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That code does not exist.");
                        pstmt.close();
                        return true;
                    }

                    pstmt = c.prepareStatement("DELETE FROM codes WHERE code=?");
                    pstmt.setString(1, code);
                    pstmt.executeUpdate();

                    pstmt = c.prepareStatement("DELETE FROM actions WHERE code=?");
                    pstmt.setString(1, code);
                    pstmt.executeUpdate();

                    pstmt = c.prepareStatement("DELETE FROM players WHERE code=?");
                    pstmt.setString(1, code);
                    pstmt.executeUpdate();
                    pstmt.close();

                    plugin.cachedCodes.remove(code);

                    sender.sendMessage(ChatColor.GOLD + "That code has been removed.");
                    return true;
                } catch (Exception e) {
                    sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                    plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                    return true;
                }
            }

            if (args[1].equals("action")) {
                if (args.length < 3) {
                    sender.sendMessage(ChatColor.GOLD + "Usage: /code remove action <actionid>");
                    return true;
                }
                String action = args[2];

                try {
                    Connection c = plugin.getConnection();
                    ResultSet rs;
                    PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM actions WHERE id=?");
                    pstmt.setString(1, action);

                    rs = pstmt.executeQuery();

                    if (rs.getInt("count(*)") < 1) {
                        sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That action does not exist.");
                        pstmt.close();
                        return true;
                    }

                    pstmt = c.prepareStatement("DELETE FROM actions WHERE id=?");
                    pstmt.setString(1, action);
                    pstmt.executeUpdate();
                    pstmt.close();

                    plugin.cachedActions.remove(action);

                    sender.sendMessage(ChatColor.GOLD + "That action has been removed.");
                    return true;
                } catch (Exception e) {
                    sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                    plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                    return true;
                }
            }

            sender.sendMessage(ChatColor.DARK_GREEN + "CustomCodes remove commands:");
            removeHelp(sender);
            return true;
        }

        if (args[0].equals("create")) {
            if (!sender.hasPermission("customcodes.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (args.length < 2) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /code create <code>");
                return true;
            }

            try {
                Connection c = plugin.getConnection();
                ResultSet rs;
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM codes WHERE code=?");
                pstmt.setString(1, args[1]);
                rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") > 0) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That code already exists.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("INSERT INTO codes (code) VALUES (?)");
                pstmt.setString(1, args[1]);
                pstmt.executeUpdate();
                pstmt.close();

                plugin.cachedCodes.add(args[1]);

                sender.sendMessage(ChatColor.GOLD + "You've created the code " + ChatColor.RED + args[1]);
                return true;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("set")) {
            if (!sender.hasPermission("customcodes.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "CustomCodes set commands:");
                setHelp(sender);
                return true;
            }

            if (args[1].equals("uses")) {
                if (args.length < 4) {
                    sender.sendMessage(ChatColor.GOLD + "Usage: /code set uses <code> <amount>");
                    return true;
                }

                String code = args[2];
                int amount;

                try {
                    amount = Integer.parseInt(args[3]);
                } catch (NumberFormatException e) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid amount.");
                    return true;
                }

                try {
                    Connection c = plugin.getConnection();
                    ResultSet rs;
                    PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM codes WHERE code=?");
                    pstmt.setString(1, code);
                    rs = pstmt.executeQuery();

                    if (rs.getInt("count(*)") < 1) {
                        sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That code does not exist.");
                        pstmt.close();
                        return true;
                    }

                    pstmt = c.prepareStatement("UPDATE codes SET uses=? WHERE code=?");
                    pstmt.setInt(1, amount);
                    pstmt.setString(2, code);
                    pstmt.executeUpdate();

                    pstmt.close();

                    sender.sendMessage(ChatColor.GOLD + "You have set the uses for the code " + ChatColor.RED + code + ChatColor.GOLD + " to " + ChatColor.RED + amount);
                    return true;
                } catch (Exception e) {
                    sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                    plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                    return true;
                }
            }

            if (args[1].equals("playercd")) {
                if (args.length < 4) {
                    sender.sendMessage(ChatColor.GOLD + "Usage: /code set playercd <code> <cooldown (1s/m/h/d)");
                    return true;
                }

                String code = args[2];
                long cooldown = plugin.stringToTime(args[3]);

                if (cooldown < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That's not a valid cooldown.");
                    return true;
                }

                try {
                    Connection c = plugin.getConnection();
                    ResultSet rs;
                    PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM codes WHERE code=?");
                    pstmt.setString(1, code);
                    rs = pstmt.executeQuery();

                    if (rs.getInt("count(*)") < 1) {
                        sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That code does not exist.");
                        pstmt.close();
                        return true;
                    }

                    pstmt = c.prepareStatement("UPDATE codes SET playerCd=? WHERE code=?");
                    pstmt.setLong(1, cooldown);
                    pstmt.setString(2, code);
                    pstmt.executeUpdate();

                    pstmt.close();
                    sender.sendMessage(ChatColor.GOLD + "The player cooldown for the code " + ChatColor.RED + code + ChatColor.GOLD + " has been set to " + plugin.convertTime(cooldown));
                    return true;
                } catch (Exception e) {
                    sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                    plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                    return true;
                }
            }

            if (args[1].equals("globalcd")) {
                if (args.length < 4) {
                    sender.sendMessage(ChatColor.GOLD + "Usage: /code set globalcd <code> <cooldown (1s/m/h/d)");
                    return true;
                }

                String code = args[2];
                long cooldown = plugin.stringToTime(args[3]);

                if (cooldown < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That's not a valid cooldown.");
                    return true;
                }

                try {
                    Connection c = plugin.getConnection();
                    ResultSet rs;
                    PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM codes WHERE code=?");
                    pstmt.setString(1, code);
                    rs = pstmt.executeQuery();

                    if (rs.getInt("count(*)") < 1) {
                        sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That code does not exist.");
                        pstmt.close();
                        return true;
                    }

                    pstmt = c.prepareStatement("UPDATE codes SET globalCd=? WHERE code=?");
                    pstmt.setLong(1, cooldown);
                    pstmt.setString(2, code);
                    pstmt.executeUpdate();

                    pstmt.close();
                    sender.sendMessage(ChatColor.GOLD + "The global cooldown for the code " + ChatColor.RED + code + ChatColor.GOLD + " has been set to " + plugin.convertTime(cooldown));
                    return true;
                } catch (Exception e) {
                    sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                    plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                    return true;
                }
            }

            if (args[1].equals("permission")) {
                if (args.length < 4) {
                    sender.sendMessage(ChatColor.GOLD + "Usage: /code set permission <code> <permission>");
                    return true;
                }

                String code = args[2];
                String permission = args[3];


                try {
                    Connection c = plugin.getConnection();
                    ResultSet rs;
                    PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM codes WHERE code=?");
                    pstmt.setString(1, code);
                    rs = pstmt.executeQuery();

                    if (rs.getInt("count(*)") < 1) {
                        sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That code does not exist.");
                        pstmt.close();
                        return true;
                    }

                    pstmt = c.prepareStatement("UPDATE codes SET permission=? WHERE code=?");
                    pstmt.setString(1, permission);
                    pstmt.setString(2, code);
                    pstmt.executeUpdate();

                    pstmt.close();
                    sender.sendMessage(ChatColor.GOLD + "The permission for the code " + ChatColor.RED + code + ChatColor.GOLD + " has been set to " + ChatColor.RED + "customcodes.code." + permission);
                    return true;
                } catch (Exception e) {
                    sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                    plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                    return true;
                }
            }

            if (args[1].equals("expires")) {
                if (args.length < 4) {
                    sender.sendMessage(ChatColor.GOLD + "Usage: /code set expires <code> <time (1s/m/h/d)");
                    return true;
                }

                String code = args[2];
                long expiresMs = plugin.stringToTime(args[3]);

                if (expiresMs < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That's not a valid time.");
                    return true;
                }

                expiresMs += System.currentTimeMillis();

                try {
                    Connection c = plugin.getConnection();
                    ResultSet rs;
                    PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM codes WHERE code=?");
                    pstmt.setString(1, code);
                    rs = pstmt.executeQuery();

                    if (rs.getInt("count(*)") < 1) {
                        sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That code does not exist.");
                        pstmt.close();
                        return true;
                    }

                    pstmt = c.prepareStatement("UPDATE codes SET expiresAt=? WHERE code=?");
                    pstmt.setLong(1, expiresMs);
                    pstmt.setString(2, code);
                    pstmt.executeUpdate();

                    pstmt.close();
                    sender.sendMessage(ChatColor.GOLD + "The expiration time for the code " + ChatColor.RED + code + ChatColor.GOLD + " has been set to " + plugin.convertTime(expiresMs - System.currentTimeMillis()));
                    return true;
                } catch (Exception e) {
                    sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                    plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                    return true;
                }
            }

            if (args[1].equals("action")) {
                if (args.length < 3) {
                    sender.sendMessage(ChatColor.DARK_GREEN + "CustomCodes set action commands:");
                    setAction(sender);
                    return true;
                }

                if (args[2].equals("cmd")) {
                    if (args.length < 5) {
                        sender.sendMessage(ChatColor.GOLD + "Usage: /code set action cmd <code> <command goes here>");
                        return true;
                    }

                    String code = args[3];
                    StringBuilder cmd = new StringBuilder();

                    for (int i=4; i<args.length; i++) {
                        cmd.append(args[i]).append(" ");
                    }

                    cmd = new StringBuilder(cmd.toString().trim());

                    try {
                        Connection c = plugin.getConnection();
                        ResultSet rs;
                        PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM codes WHERE code=?");
                        pstmt.setString(1, code);
                        rs = pstmt.executeQuery();

                        if (rs.getInt("count(*)") < 1) {
                            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That code does not exist.");
                            return true;
                        }

                        String randomId = plugin.randomId();

                        pstmt = c.prepareStatement("INSERT INTO actions (id, code, type, action) VALUES (?, ?, ?, ?)");
                        pstmt.setString(1, randomId);
                        pstmt.setString(2, code);
                        pstmt.setString(3, "cmd");
                        pstmt.setString(4, cmd.toString());
                        pstmt.executeUpdate();
                        pstmt.close();

                        plugin.cachedActions.add(randomId);

                        sender.sendMessage(ChatColor.GOLD + "You've added an action to the code " + ChatColor.RED + code);
                        return true;
                    } catch (Exception e) {
                        sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                        plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                        e.printStackTrace();
                        return true;
                    }
                }

                if (args[2].equals("msg")) {
                    if (args.length < 5) {
                        sender.sendMessage(ChatColor.GOLD + "Usage: /code set action msg <code> <message goes here>");
                        return true;
                    }

                    String code = args[3];
                    StringBuilder msg = new StringBuilder();

                    for (int i=4; i<args.length; i++) {
                        msg.append(args[i]).append(" ");
                    }

                    msg = new StringBuilder(msg.toString().trim());

                    try {
                        Connection c = plugin.getConnection();
                        ResultSet rs;
                        PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM codes WHERE code=?");
                        pstmt.setString(1, code);
                        rs = pstmt.executeQuery();

                        if (rs.getInt("count(*)") < 1) {
                            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That code does not exist.");
                            return true;
                        }

                        String randomId = plugin.randomId();

                        pstmt = c.prepareStatement("INSERT INTO actions (id, code, type, action) VALUES (?, ?, ?, ?)");
                        pstmt.setString(1, randomId);
                        pstmt.setString(2, code);
                        pstmt.setString(3, "msg");
                        pstmt.setString(4, msg.toString());
                        pstmt.executeUpdate();
                        pstmt.close();

                        plugin.cachedActions.add(randomId);

                        sender.sendMessage(ChatColor.GOLD + "You've added an action to the code " + ChatColor.RED + code);
                        return true;
                    } catch (Exception e) {
                        sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                        plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                        e.printStackTrace();
                        return true;
                    }
                }

                if (args[2].equals("item")) {
                    if (!(sender instanceof Player)) {
                        sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                        return true;
                    }

                    if (args.length < 4) {
                        sender.sendMessage(ChatColor.GOLD + "Usage: /code set action item <code>");
                        return true;
                    }

                    Player player = (Player) sender;
                    String code = args[3];

                    ItemStack item = player.getInventory().getItemInMainHand();
                    if (item == null || item.getType().equals(Material.AIR)) {
                        player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You're not holding an item.");
                        return true;
                    }

                    if (item.hasItemMeta()) {
                        ItemMeta meta = item.getItemMeta();
                        NamespacedKey key = NamespacedKey.minecraft("53254");
                        Enchantment enchant = Enchantment.getByKey(key);
                        if (enchant != null && meta.hasEnchant(enchant)) {
                            meta.removeEnchant(enchant);
                            meta.addEnchant(Enchantment.DURABILITY, 3, false);
                            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                        }

                        if (meta.hasLore()) {
                            List<String> lore = meta.getLore();
                            for (int i = 0; i < lore.size(); i++) {
                                String curLore = lore.get(i);
                                curLore = curLore.replaceAll("\\{user}", player.getName());
                                curLore = curLore.replaceAll("\\{player}", player.getName());
                                curLore = curLore.replaceAll("\\{name}", player.getName());
                                lore.set(i, curLore);
                            }
                            meta.setLore(lore);
                        }

                        item.setItemMeta(meta);
                    }

                    try {
                        Connection c = plugin.getConnection();
                        ResultSet rs;
                        PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM codes WHERE code=?");
                        pstmt.setString(1, code);
                        rs = pstmt.executeQuery();

                        if (rs.getInt("count(*)") < 1) {
                            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That code does not exist.");
                            pstmt.close();
                            return true;
                        }

                        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                        BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

                        dataOutput.writeObject(item);
                        dataOutput.close();

                        String randomId = plugin.randomId();

                        pstmt = c.prepareStatement("INSERT INTO actions (id, code, type, action) VALUES (?, ?, ?, ?)");
                        pstmt.setString(1, randomId);
                        pstmt.setString(2, code);
                        pstmt.setString(3, "item");
                        pstmt.setString(4, Base64Coder.encodeLines(outputStream.toByteArray()));
                        pstmt.executeUpdate();
                        pstmt.close();

                        plugin.cachedActions.add(randomId);

                        sender.sendMessage(ChatColor.GOLD + "That item has been added to the code " + ChatColor.RED + code);
                        return true;
                    } catch (Exception e) {
                        sender.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
                        plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                        e.printStackTrace();
                        return true;
                    }
                }

                sender.sendMessage(ChatColor.DARK_GREEN + "CustomCode set action commands:");
                setAction(sender);
                return true;
            }

            sender.sendMessage(ChatColor.DARK_GREEN + "CustomCode set commands:");
            setHelp(sender);
            return true;
        }

        sender.sendMessage(ChatColor.DARK_GREEN + "CustomCodes commands:");
        sender.sendMessage(ChatColor.GOLD + "/code redeem <code> - Redeems a code.");
        if (sender.hasPermission("customcodes.admin")) {
            sender.sendMessage(ChatColor.GOLD + "/code create <code> - Creates a new code.");
            listHelp(sender);
            removeHelp(sender);
            setHelp(sender);
        }
        return true;
    }

    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        List<String> tabComplete = new ArrayList<>();
        if (args.length == 1) {
            tabComplete.add("redeem");
            if (sender.hasPermission("customcodes.admin")) {
                tabComplete.add("create");
                tabComplete.add("remove");
                tabComplete.add("list");
                tabComplete.add("set");
            }
        }
        if (args.length == 2 && sender.hasPermission("customcodes.admin")) {
            if (args[0].equals("create")) tabComplete.add("<code>");
            if (args[0].equals("redeem")) tabComplete.add("<code>");
            if (args[0].equals("list")) {
                tabComplete.add("codes");
                tabComplete.add("actions");
            }
            if (args[0].equals("remove")) {
                tabComplete.add("code");
                tabComplete.add("action");
            }
            if (args[0].equals("set")) {
                tabComplete.add("uses");
                tabComplete.add("playercd");
                tabComplete.add("globalcd");
                tabComplete.add("expires");
                tabComplete.add("permission");
                tabComplete.add("action");
            }
        }
        if (args.length == 3 && sender.hasPermission("customcodes.admin")) {
            if (args[0].equals("list")) {
                if (args[1].equals("actions")) tabComplete.addAll(plugin.cachedCodes);
            }
            if (args[0].equals("remove")) {
                if (args[1].equals("code")) tabComplete.addAll(plugin.cachedCodes);
                if (args[1].equals("action")) tabComplete.addAll(plugin.cachedActions);
            }
            if (args[0].equals("set")) {
                String[] validCommands = {"uses", "playercd", "globalcd", "expires", "permission"};
                if (Arrays.asList(validCommands).contains(args[1])) {
                    tabComplete.addAll(plugin.cachedCodes);
                }
                if (args[1].equals("action")) {
                    tabComplete.add("cmd");
                    tabComplete.add("item");
                    tabComplete.add("msg");
                }
            }
        }
        if (args.length == 4 && sender.hasPermission("customcodes.admin")) {
            if (args[0].equals("set")) {
                if (args[1].equals("uses")) tabComplete.add("<amount>");
                if (args[1].equals("playercd") || args[1].equals("globalcd")) tabComplete.add("<cooldown>");
                if (args[1].equals("expires")) tabComplete.add("<time>");
                if (args[1].equals("permission")) tabComplete.add("<permission>");
                if (args[1].equals("action")) tabComplete.addAll(plugin.cachedCodes);
            }
        }
        if (args.length == 5 && sender.hasPermission("customcodes.admin")) {
            if (args[0].equals("set") && args[1].equals("action")) {
                if (args[2].equals("cmd")) tabComplete.add("<eco give {user} 500>");
                if (args[2].equals("msg")) tabComplete.add("<msg>");
            }
        }
        return (args.length > 0) ? StringUtil.copyPartialMatches(args[args.length - 1], tabComplete, new ArrayList<>()) : null;
    }

    private void listHelp(CommandSender sender) {
        sender.sendMessage(ChatColor.GOLD + "/code list codes - Lists all codes.");
        sender.sendMessage(ChatColor.GOLD + "/code list actions <code> - Lists all actions for a code.");
    }

    private void removeHelp(CommandSender sender) {
        sender.sendMessage(ChatColor.GOLD + "/code remove code <code> - Deletes a code.");
        sender.sendMessage(ChatColor.GOLD + "/code remove action <actionid> - Deletes an action from a code.");
    }

    private void setHelp(CommandSender sender) {
        sender.sendMessage(ChatColor.GOLD + "/code set uses <code> <amount> - Sets the number of times this code can be redeemed.");
        sender.sendMessage(ChatColor.GOLD + "/code set playercd <code> <cooldown (1s/m/h/d)> - Sets the player cooldown for this code.");
        sender.sendMessage(ChatColor.GOLD + "/code set globalcd <code> <cooldown (1s/m/h/d)> - Sets the global cooldown for this code.");
        sender.sendMessage(ChatColor.GOLD + "/code set expires <code> <time (1s/m/h/d)> - Sets when this code expires and can no longer be redeemed.");
        sender.sendMessage(ChatColor.GOLD + "/code set permission <code> <permission> - Sets the permission required to redeem this code.");
        setAction(sender);
    }
    private void setAction(CommandSender sender) {
        sender.sendMessage(ChatColor.GOLD + "/code set action cmd <code> <give {user/player/name} dirt 64> - Adds an action to this code. {user}, {player}, and {name} will all be replaced with the players username.");
        sender.sendMessage(ChatColor.GOLD + "/code set action item <code> - Adds the item in your hand to this code.");
        sender.sendMessage(ChatColor.GOLD + "/code set action msg <code> <msg here> - Adds a message to this code.");
    }
}
