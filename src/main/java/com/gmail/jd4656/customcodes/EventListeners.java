package com.gmail.jd4656.customcodes;

import com.gmail.jd4656.InventoryManager.InventoryClickHandler;
import com.gmail.jd4656.InventoryManager.InventoryManager;
import net.minecraft.server.v1_16_R3.NBTTagCompound;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.craftbukkit.v1_16_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;

public class EventListeners implements Listener {
    private CustomCodes plugin;

    EventListeners(CustomCodes p) {
        plugin = p;
    }

    @EventHandler
    public void PlayerInteractEvent(PlayerInteractEvent event) {
        Action action = event.getAction();
        Player player = event.getPlayer();
        ItemStack item = event.getItem();
        String uuid = player.getUniqueId().toString();
        if (!action.equals(Action.RIGHT_CLICK_BLOCK) && !action.equals(Action.RIGHT_CLICK_AIR)) return;
        if (item == null) return;

        net.minecraft.server.v1_16_R3.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
        NBTTagCompound itemCompound = (nmsItem.hasTag() ? nmsItem.getTag() : new NBTTagCompound());
        String voucherId = itemCompound.getString("cvId");
        String redeemableBy = itemCompound.getString("cvRedeemableBy");

        if (voucherId == null || voucherId.equals("")) return;
        if (!player.hasPermission("customcodes.use")) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to redeem vouchers.");
            return;
        }
        if (redeemableBy != null && !redeemableBy.equals("")) {
            OfflinePlayer voucherOwner = Bukkit.getOfflinePlayer(UUID.fromString(redeemableBy));
            if (!player.getUniqueId().equals(voucherOwner.getUniqueId())) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This voucher is only redeemable by " + ChatColor.GOLD + voucherOwner.getName());
                return;
            }
        }

        try {
            Connection c = plugin.getConnection();
            PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM vouchers WHERE id=?");
            pstmt.setString(1, voucherId);
            ResultSet rs = pstmt.executeQuery();

            if (rs.getInt("count(*)") < 1) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This voucher is no longer valid.");
                pstmt.close();
                return;
            }

            pstmt = c.prepareStatement("SELECT * FROM vouchers WHERE id=?");
            pstmt.setString(1, voucherId);
            rs = pstmt.executeQuery();

            String voucherType = rs.getString("type");
            String permission = rs.getString("permission");
            String permTime = rs.getString("permTime");
            String command = rs.getString("command");

            if (voucherType.equals("command")) {
                if (command == null) {
                    player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "The command for this voucher has not been set.");
                    pstmt.close();
                    return;
                }

                if (!player.getInventory().contains(item)) {
                    player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You don't have this voucher in your inventory anymore.");
                    pstmt.close();
                    return;
                }

                command = command.replaceAll("\\{user}", player.getName());
                command = command.replaceAll("\\{player}", player.getName());
                command = command.replaceAll("\\{name}", player.getName());

                ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
                Bukkit.dispatchCommand(console, command);

                plugin.removeItems(player.getInventory(), item, 1);

                pstmt.close();

                plugin.log(player.getName() + " has redeemed a voucher: " + voucherId);

                player.sendMessage(ChatColor.GOLD + "You've redeemed this voucher.");
                return;
            }

            if (voucherType.equals("permission")) {
                if (permission == null) {
                    player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "The permission for this voucher has not been set.");
                    pstmt.close();
                    return;
                }

                if (!player.getInventory().contains(item)) {
                    player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You don't have this voucher in your inventory anymore.");
                    pstmt.close();
                    return;
                }

                String permissionString = "lp user " + player.getName() + " permission set " + permission;

                if (permTime != null) {
                    permissionString = "lp user " + player.getName() + " permission settemp " + permission + " true " + permTime + " accumulate";
                }

                ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
                Bukkit.dispatchCommand(console, permissionString);

                plugin.removeItems(player.getInventory(), item, 1);

                plugin.log(player.getName() + " has redeemed a voucher: " + voucherId);
                player.sendMessage(ChatColor.GOLD + "You've redeemed this voucher.");
                pstmt.close();
                return;
            }

            if (voucherType.equals("item")) {
                pstmt = c.prepareStatement("SELECT count(*) FROM voucherItems WHERE voucher=?");
                pstmt.setString(1, voucherId);
                rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This voucher has no items added.");
                    pstmt.close();
                    return;
                }

                InventoryManager manager = new InventoryManager(ChatColor.RED + ChatColor.BOLD.toString() + "Click an item to redeem", 54, plugin);

                pstmt = c.prepareStatement("SELECT * FROM voucherItems WHERE voucher=?");
                pstmt.setString(1, voucherId);
                rs = pstmt.executeQuery();

                int count = 0;
                while (rs.next()) {
                    ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(rs.getString("item")));
                    BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);

                    ItemStack curItem = (ItemStack) dataInput.readObject();
                    dataInput.close();

                    manager.withItem(count, curItem);
                    count++;
                }

                pstmt.close();

                manager.withEventHandler(new InventoryClickHandler() {
                    @Override
                    public void handle(InventoryClickEvent event) {
                        Player player = (Player) event.getWhoClicked();
                        ItemStack clicked = event.getCurrentItem();
                        if (clicked.getType().equals(Material.AIR)) {
                            event.setCancelled(true);
                            return;
                        }

                        InventoryManager confirm = new InventoryManager(ChatColor.GOLD + "Confirm voucher selection", 54, plugin);

                        ItemStack yes = new ItemStack(Material.LIME_CONCRETE, 1);
                        ItemMeta yesMeta = yes.getItemMeta();
                        yesMeta.setDisplayName("Confirm");
                        yes.setItemMeta(yesMeta);

                        ItemStack no = new ItemStack(Material.RED_CONCRETE, 1);
                        ItemMeta noMeta = yes.getItemMeta();
                        noMeta.setDisplayName("Cancel");
                        no.setItemMeta(noMeta);

                        confirm.withItem(0, clicked);
                        confirm.withItem(1, yes);
                        confirm.withItem(2, no);

                        confirm.withEventHandler(new InventoryClickHandler() {
                            @Override
                            public void handle(InventoryClickEvent event1) {
                                Player player2 = (Player) event1.getWhoClicked();
                                ItemStack clicked1 = event1.getCurrentItem();
                                if (!clicked1.getType().equals(Material.LIME_CONCRETE) && !clicked1.getType().equals(Material.RED_CONCRETE)) {
                                    event1.setCancelled(true);
                                    return;
                                }

                                if (clicked1.getType().equals(Material.LIME_CONCRETE)) {
                                    if (!player.getInventory().contains(item)) {
                                        player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You don't have this voucher in your inventory anymore.");
                                        player.closeInventory();
                                        return;
                                    }
                                    plugin.removeItems(player.getInventory(), item, 1);

                                    player.getInventory().addItem(clicked);
                                    player.closeInventory();

                                    plugin.log(player.getName() + " has redeemed a voucher: " + voucherId);
                                    player.sendMessage(ChatColor.GOLD + "You've redeemed this voucher.");
                                }

                                if (clicked1.getType().equals(Material.RED_CONCRETE)) player.closeInventory();
                            }
                        });

                        confirm.show(player);
                    }
                });

                manager.show(player);
            }
        } catch (Exception e) {
            player.sendMessage(ChatColor.DARK_RED + "Error querying the database.");
            plugin.getLogger().severe("PlayerInteractEvent: " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
        }
    }
}
